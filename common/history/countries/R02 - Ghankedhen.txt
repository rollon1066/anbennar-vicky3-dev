﻿COUNTRIES = {
	c:R02 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged
	}
}