﻿COUNTRIES = {
	c:L08 = {
		effect_starting_technology_baashidi_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slave_trade
		activate_law = law_type:law_poor_laws

	}
}