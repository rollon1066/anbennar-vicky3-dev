﻿DIPLOMACY = {
	c:A30 = {	#Demesne-Wyvernheart vs Grombari Aggression
		create_diplomatic_pact = {
			country = c:A26
			type = defensive_pact
		}	
	}	

	c:A27 = {	#Three Kings Alliance historically vs Anbennar for independence
		create_diplomatic_pact = {
			country = c:A28
			type = defensive_pact
		}	
		create_diplomatic_pact = {
			country = c:A29
			type = defensive_pact
		}
	}
	c:A28 = {	#Three Kings Alliance historically vs Anbennar for independence
		create_diplomatic_pact = {
			country = c:A29
			type = defensive_pact
		}	
	}

	c:D05 = {	#Amldihr and Krakdhumvror vs Grombar
		create_diplomatic_pact = {
			country = c:D02
			type = defensive_pact
		}
	}

	c:D31 = {	#Dak and Nadimraj vs Underkingdom
		create_diplomatic_pact = {
			country = c:R07
			type = defensive_pact
		}
	}

	c:B42 = {	#Southern Expanse bloc formed against Plumstead, later joined by Plumstead after Sarda resurgence (very unstable cause they hate each other but need each other)
		create_diplomatic_pact = {
			country = c:B34
			type = defensive_pact
		}	
		create_diplomatic_pact = {
			country = c:B41
			type = defensive_pact
		}
	}
}