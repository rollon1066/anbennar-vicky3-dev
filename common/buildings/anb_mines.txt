﻿building_damestear_mine = {
	building_group = bg_damestear_mining
	texture = "gfx/interface/icons/building_icons/damestear_mine.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		prospecting
	}

	production_method_groups = {
		pmg_mining_equipment_building_damestear_mine
		pmg_explosives_building_damestear_mine
		pmg_steam_automation_building_damestear_mine
		pmg_train_automation_building_damestear_mine
		pmg_ownership_capital_building_damestear_mine
	}
}

building_damestear_fields = {
	building_group = bg_damestear_fields
	texture = "gfx/interface/icons/building_icons/damestear_fields.dds"
	city_type = mine
	levels_per_mesh = 5
	buildable = no
	expandable = no
	terrain_manipulator = mining
	
	unlocking_technologies = {
		prospecting
		damestear_meteorology
	}

	production_method_groups = {
		pmg_base_building_damestear_fields
	}
}
