﻿pm_merchant_guilds_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_picks_and_shovels_building_damestear_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}

pm_privately_owned_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_publicly_traded_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	unlocking_technologies = {
		mutual_funds
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 150
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_government_run_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"

	unlocking_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 150
		}
		unscaled = {
			building_government_shares_add = 1
		}
	}
}

pm_worker_cooperative_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/worker_cooperative.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}

	unlocking_laws = {
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 250
		}
		unscaled = {
			building_workforce_shares_add = 1
		}
	}
}

pm_picks_and_shovels_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_damestear_add = 10
		}

		level_scaled = {
			building_employment_laborers_add = 4500
		}
	}
}

pm_atmospheric_engine_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_damestear_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}

pm_condensing_engine_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_damestear_add = 25
		}

		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 250
		}
	}
}

pm_diesel_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		combustion_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 10
			
			# output goods
			goods_output_damestear_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 500
		}
	}
}

pm_nitroglycerin_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
	 	nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_damestear_add = 5
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}

pm_dynamite_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_damestear_add = 10
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

default_building_damestear_fields = {
	texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_damestear_add = 20
		}
		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
		unscaled = {
			building_shopkeepers_shares_add = 3
			building_laborers_shares_add = 1
		}
	}
}
