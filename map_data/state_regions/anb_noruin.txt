STATE_KWINELLIANDE = {
    id = 225
    subsistence_building = "building_subsistence_farms"
    provinces = { "x105BA8" "x1324AC" "x1A8C7E" "x3A21D2" "x5B98AC" "x625917" "x7100F4" "x7300E8" "x7400EC" "x777DF5" "x7DCF00" "x9371C9" "x998A4F" "xA74A4F" "xA74EAD" "xBBA963" "xD3042C" "xD4A781" "xE9B195" "xEF8010" "xF08FCF" "xF22BCA" "xFF7DD7" }
    traits = { state_trait_bloodgroves state_trait_sella_river }
    city = "x998a4f" #Sella


    port = "x1a8c7e" #Kwinehcost


    farm = "x7400ec" #Dreonn


    wood = "xf08fcf" #Gabenn


    arable_land = 146
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 15
        bg_iron_mining = 30
    }
    naval_exit_id = 3101
}

STATE_NURTHANNCOST = {
    id = 226
    subsistence_building = "building_subsistence_farms"
    provinces = { "x049691" "x285020" "x28547E" "x593C7C" "x6EB158" "x6EDC33" "x722D2B" "x822397" "xA1C031" "xA8524F" "xAB8E24" "xBBDE2E" "xE183A7" "xECB39A" "xF9E577" }
    traits = { state_trait_bloodgroves }
    city = "xbbde2e" #Abhenn


    port = "x28547e" #Jertrincost


    farm = "xa8524f" #Aggolinn


    mine = "x285020" #Kormistvern


    wood = "x593c7c" #NEW PLACE


    arable_land = 76
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 11
    }
    naval_exit_id = 3101
}

STATE_DEATHWOODS = {
    id = 227
    subsistence_building = "building_subsistence_farms"
    provinces = { "x056575" "x11E04B" "x19847E" "x29A606" "x304688" "x31058F" "x4F83F6" "x537183" "x73FF12" "x8976A7" "x9148CC" "xA856AD" "xA85A4F" "xCF832E" "xD5E26A" "xE0F784" "xEBA1B5" }
    traits = { state_trait_bloodgroves }
    city = "x304688" #Sennsect


    port = "xcf832e" #Firrfrem


    farm = "xa85a4f" #Abhennard


    wood = "x4f83f6" #Cann Silsunvern


    arable_land = 87
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 20
        bg_iron_mining = 24
    }
    naval_exit_id = 3108 #Trollsbay


}

STATE_SORNICANDE = {
    id = 228
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1383F6" "x15015F" "x1B3858" "x1C8BCA" "x1EC8FC" "x274820" "x29647E" "x2B5897" "x2D3E46" "x32D1A2" "x4178C7" "x427820" "x43966D" "x48067B" "x6AA425" "x6FD2C8" "x7000F0" "x73EC48" "x80D0E5" "x8C91D6" "x9AAF3A" "x9B0A7A" "x9DE42A" "xA37A79" "xA9624F" "xAAA21B" "xBF4E34" "xC1A1C8" "xD78C29" "xDAE7CA" "xF06DCE" "xF5005C" "xF63FA0" }
    traits = { state_trait_bloodgroves state_trait_sella_river }
    city = "x1383f6" #Fiorjert


    farm = "xaaa21b" #Fadakwinn


    mine = "xd78c29" #Imelvars


    wood = "x7000f0" #Sellacann


    arable_land = 84
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 23
        bg_iron_mining = 17
        bg_coal_mining = 16
    }
}

STATE_SOUTH_DALAIRE = {
    id = 229
    subsistence_building = "building_subsistence_farms"
    provinces = { "x084C64" "x198820" "x257F09" "x285C7E" "x2B0A7D" "x31FA4F" "x3724A0" "x38416E" "x5635F0" "x627AEA" "x728DA4" "x7B3596" "x8EB4D8" "x9FC353" "xA48CDD" "xA95EAD" "xAE21D8" "xCDC856" "xE95E00" "xE9F823" "xF6543C" "xFF11A5" }
    traits = { state_trait_bloodgroves }
    city = "x198820" #Fiorcog


    port = "xe95e00" #Estadaran


    farm = "x3724a0" #Fiashilsun


    mine = "xa95ead" #Frostwoods


    wood = "xf6543c" #Mattenvars


    arable_land = 48
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 3
        bg_logging = 17
        bg_sulfur_mining = 32
    #also 1 diamonds and uranium deposit
    }
    naval_exit_id = 3101
}

STATE_CESTIRMARK = {
    id = 230
    subsistence_building = "building_subsistence_farms"
    provinces = { "x097315" "x0B1F9F" "x0D41EF" "x18D673" "x1A9020" "x1A9820" "x554820" "x6D00F8" "x877A90" "x8C0430" "x8ED295" "x95937D" "x9A924F" "xA96A4F" "xB599A4" "xB92B2A" "xCD1A61" "xCD577B" "xD8A8DF" "xECE3CA" "xEF4ABA" "xFD17A8" }
    traits = {}
    city = "x554820" #Cestirmark


    port = "xb92b2a" #Vincenton


    farm = "x1a9820" #Arangreen


    wood = "xece3ca" #Lancewood


    arable_land = 142
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 12
        bg_logging = 17
        bg_iron_mining = 27
    }
    naval_exit_id = 3108 #Trollsbay


}

STATE_UPPER_SELLA = {
    id = 231
    subsistence_building = "building_subsistence_farms"
    provinces = { "x02EA6D" "x0FDEFC" "x102D34" "x1DCACA" "x296820" "x2A6C7E" "x3670A5" "x5BAEBD" "x604E63" "x69730E" "x811307" "x81445F" "x83F120" "x9AC48B" "xA150BA" "xAA6EAD" "xAFD21C" "xB2B547" "xBCEA04" "xBD565B" "xCC7056" "xCDD6B3" "xDB24D4" }
    traits = { state_trait_sella_river }
    city = "x296820" #Yanekin


    farm = "x81445f" #NEW PLACE


    mine = "x02ea6d" #Arbeloch


    wood = "x811307" #Bentwaters


    arable_land = 67
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 21
        bg_coal_mining = 10
        bg_sulfur_mining = 16
    #also 1 diamonds deposit
    }
}

STATE_TROLLSBRIDGE = {
    id = 232
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1A947E" "x23D587" "x4324CA" "x51905F" "x598624" "x62F2C5" "x65D9BB" "x6C00F4" "x97AAC9" "x9A96AD" "x9F524F" "xF01B58" "xFC9111" }
    traits = {}
    city = "x23d587" #Pelletton


    port = "x1a947e" #Trollsbridge


    farm = "x598624" #NEW PLACE


    wood = "xf01b58" #Fernwell


    arable_land = 61
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 6
        bg_iron_mining = 36
    }
    naval_exit_id = 3108
}

STATE_SOUTH_MARLLIANDE = {
    id = 233
    subsistence_building = "building_subsistence_farms"
    provinces = { "x010091" "x1B2020" "x1B9C7E" "x1D136D" "x4900F4" "x4C00EC" "x4D8F7B" "x5974CE" "x5E60C2" "x9A3865" "x9B224F" "x9B9A4F" "xA6B456" "xA81309" "xAE5644" "xBF7BF6" "xC01047" "xC46499" "xD24F76" "xD2ECA0" "xE2EDD4" }
    traits = {}
    city = "x5e60c2" #Marlliande


    port = "xbf7bf6" #New Varlosen


    farm = "x9b9a4f" #New Endersby


    wood = "xd24f76" #Eskamton


    arable_land = 138
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 17
    }
    naval_exit_id = 3108
}
STATE_NORTH_MARLLIANDE = {
    id = 234
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2A747E" "x350E3C" "x428AF3" "x4A20B5" "x4E00F4" "x5F9E26" "x610DB3" "x8C83C4" "x8D6F26" "x8EDB2A" "x923F7C" "x9B9EAD" "xAA724F" "xAF0637" "xE1CE5E" "xE5C8C4" "xEB9C26" }
    traits = { state_trait_sella_river }
    city = "x8c83c4" #NEW PLACE


    farm = "xeb9c26" #Bramblebank


    mine = "x9b9ead" #Gateview


    wood = "xaf0637" #Sella Falls


    arable_land = 76
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 10
        bg_lead_mining = 12
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 2
    }
}

STATE_YNNSMOUTH = {
    id = 235
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x1B247E" "x3368E8" "x5168BF" "x554C7E" "x835FB9" "x9C2A4F" "xD55E8D" }
    traits = { state_trait_ynn_river state_trait_venaans_tears }
    city = "xd55e8d" #Inekshut


    port = "x1b247e" #Ynnsmouth


    farm = "x5168bf" ##NEW PLACE


    mine = "x835FB9" #Ruincliff


    wood = "x554c7e" #Venainé


    arable_land = 56
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 6
        bg_logging = 8
        bg_lead_mining = 21
    #also 3 bauxite deposits and 1 uranium deposit
    }
    naval_exit_id = 3108
}

STATE_ZANLIB = {
    id = 236
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1C2820" "x1C2C7E" "x4600E8" "x4AC96F" "x544020" "x5A706A" "x7A0283" "x7E7867" "x95157C" "x99E6CC" "x9B26AD" "x9D3EAD" "xA08309" "xA73555" "xED2E28" }
    traits = {}
    city = "x5a706a" #Zankumar


    port = "x99e6cc" #Ahitušes


    farm = "x95157c" #Aqatzan


    mine = "x1c2820" #Nabakelu


    wood = "x1c2c7e" #Umuhitu


    arable_land = 121
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 14
        bg_lead_mining = 24
        bg_coal_mining = 36
    #also 1 bauxite deposit
    }
    naval_exit_id = 3108
}

STATE_ISOBELIN = {
    id = 237
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x13C522" "x1C3020" "x1C347E" "x1D4020" "x23BCCA" "x273ACD" "x4500F8" "x4700EC" "x4B00E8" "x639CB4" "x9C2EAD" "x9C324F" }
    traits = { state_trait_natural_harbors state_trait_lady_isobel }
    city = "x9c324f" #Port Isobel


    port = "x1c347e" #Isobel Island


    farm = "x4b00e8" #Smallmouth


    mine = "x9c2ead" #Fort Thorn


    wood = "x1d4020" #Trasilden


    arable_land = 109
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_coffee_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 12
        bg_logging = 14
        bg_lead_mining = 19
        bg_coal_mining = 40
    #also 1 diamond deposit and 1 bauxite deposit
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 2
    }
    naval_exit_id = 3108
}

STATE_THILVIS = {
    id = 238
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00150B" "x2496B3" "x508633" "x5FC494" "x621FC4" "x9D36AD" "x9D3A4F" "xA7D0F0" "xD43EAD" "xDAF54D" }
    traits = {}
    city = "x9D36AD" #Thilvis


    port = "x00150B" #Totemsglade


    farm = "x5FC494" #Primslodge


    wood = "x621FC4" #Stillwell


    arable_land = 137
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 14
        bg_iron_mining = 60
        bg_coal_mining = 32
    }
    naval_exit_id = 3108
}

STATE_BOEK = {
    id = 239
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1D3820" "x1DFE7E" "x1E447E" "x3400F0" "x4B3B73" "x585BF5" "x5B721F" "x5CE953" "x82001D" "x8788F2" "x9D424F" "xD40DA0" "xD65A4F" }
    traits = {}
    city = "xd65a4f" #Boekshire


    farm = "x1e447e" #Cecillcreek


    mine = "x4b3b73" #Edmundspring


    wood = "x1dfe7e" #Rimsby


    arable_land = 68
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 22
        bg_iron_mining = 36
        bg_coal_mining = 24
    }
}

STATE_VALORPOINT = {
    id = 240
    subsistence_building = "building_subsistence_farms"
    provinces = { "x191FC1" "x1E4C7E" "x1E5020" "x1EF320" "x2D3310" "x2F36D4" "x450D66" "x46C3FA" "x54447E" "x866F9C" "x8735F5" "x8A3A2D" "x9E4EAD" "x9EAE4F" "xC4695C" "xFB0024" }
    traits = {}
    city = "x9EAE4F" #Armocsport


    port = "x9E4EAD" #Cape Deryk


    farm = "x1EF320" #Laneksfield


    wood = "xFB0024" #Greywood


    arable_land = 106
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 16
        bg_iron_mining = 27
    }
    naval_exit_id = 3108
}

STATE_OKHIBOLI = {
    id = 241
    subsistence_building = "building_subsistence_farms"
    provinces = { "x034753" "x09C034" "x2A6C06" "x30937E" "x31FC79" "x4100FA" "x4B0019" "x547667" "x5500E1" "x7300AF" "x89BAB7" "xDA28ED" "xF10A5A" }
    traits = {}
    city = "x5500E1" #Shofashok


    port = "x4B0019" #Okhispier


    farm = "x4100FA" #Soupspoone


    mine = "x31fc79" #NEW PLACE


    wood = "x7300AF" #Okhiboli


    arable_land = 72
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 10
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3106
}

STATE_SPOORLAND = {
    id = 242
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A3C8D" "x147190" "x17DEFC" "x2398F2" "x3200E8" "x370000" "x387A9D" "x4200EC" "x483C9E" "x90D0DB" "x987E4C" "x9C9CB2" "xA6C8D3" "xAE6188" "xB07540" "xB9B933" "xBE7C02" "xDB42AA" "xFCB36B" "xFD9EB8" }
    traits = {}
    city = "x3200e8" #Nashoba


    farm = "x483c9e" #Spoorland Gap


    wood = "x370000" #Spoorland


    arable_land = 89
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 12
        bg_iron_mining = 30
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
}

STATE_GRAVEROADS = {
    id = 243
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0BA388" "x12CF2B" "x4400F4" "x449075" "x543C7E" "x5F0032" "x61A22C" "x82F8E3" "x8AFC1C" "xB55FD9" "xC4BA5A" "xD4E837" "xDE64D9" "xE08798" "xE1BFC8" }
    traits = {}
    city = "x449075" #Bonebridge


    port = "xD4E837" #Achukkoa


    farm = "x61A22C" #The Wake


    wood = "xDE64D9" #Evermore


    arable_land = 61
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 14
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
    naval_exit_id = 3106
}

STATE_NERATICA = {
    id = 244
    subsistence_building = "building_subsistence_farms"
    provinces = { "x149D0B" "x4100E8" "x433F3F" "x6C8722" "x6E004B" "x780064" "x8C967B" "xAC92C0" "xCF5004" "xD97708" }
    traits = {}
    city = "x4100e8" #Cheshosh'tchuka


    port = "x6e004b" #Altarcliff


    farm = "x6c8722" #Pale Coast


    wood = "xe6a9a8" #Redrushes


    arable_land = 93
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 3
        bg_logging = 10
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 6
    }
    naval_exit_id = 3106
}

STATE_DISHENTI = {
    id = 245
    subsistence_building = "building_subsistence_farms"
    provinces = { "x009717" "x15CACA" "x17F066" "x279BE1" "x3ADB00" "x4000F8" "x42844D" "x4A97B1" "x55D35B" "x62C5DE" "x6762B2" "x6900C8" "x6BA627" "x77F468" "x7A7D3E" "x7E27A2" "xAA1C55" "xAE5924" "xB3C314" "xBAEC3C" "xDCF7D4" "xE7BB54" "xED9A4F" "xEDFCE8" "xF15F50" "xF82903" "xFF12F7" }
    traits = {}
    city = "x6BA627" #Haitawrahpo


    farm = "xae5924" #NEW PLACE


    mine = "x4a97b1" #NEW PLACE


    wood = "x15caca" #Finaltoll


    arable_land = 51
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 18
        bg_lead_mining = 18
    #also 1 uranium deposit
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 8
    }
}
STATE_DARKGROVES = {
    id = 246
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D010B" "x153D61" "x1BDEFC" "x3EDA5E" "x3F00F4" "x43A7DB" "x5AA879" "x5F3135" "x6B3184" "x7AAB2D" "x7D0096" "x902D34" "xBFA3E9" "xC655DE" "xC80925" "xD546AD" "xD5524F" "xE6A9A8" "xF639B8" }
    traits = {}
    city = "x3f00f4" #Onyxgard


    port = "xd546ad" #Maskport


    farm = "x0d010b" #Hotchakkoa


    wood = "x3eda5e" #The Impenetrable


    arable_land = 38
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 7
        bg_logging = 7
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 8
    }
    naval_exit_id = 3106
}
STATE_MORDUN = {
    id = 247
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0B96E0" "x201AEE" "x2A7020" "x2B707C" "x390D18" "x3E8D6F" "x42E495" "x4D00F0" "x503022" "x56547E" "x62B6F9" "x6C1BF4" "x6D10B9" "x6F8AE2" "x8EB67D" "x97CFA5" "x9DBE8A" "xAB3C4A" "xC36738" "xD4424F" "xDAA9F0" }
    traits = {}
    city = "x62B6F9" #Wisichi


    port = "x390D18" #Spit of Fate


    farm = "x4D00F0" #NEW PLACE


    mine = "xd4424f" #NEW PLACE


    wood = "x56547E" #Ruby Drop


    arable_land = 46
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 6
        bg_iron_mining = 24
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 8
    }
    naval_exit_id = 3106
}
